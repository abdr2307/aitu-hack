package smtp

import (
	"bytes"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)
const smtpServer = "localhost:5001"

type ClientSMTP struct {
	auth sasl.Client
	smtpServer string
	from string
}

func NewClientSMTP(email, pwd string, cfg *ServerSMTPConfig) *ClientSMTP {
	return &ClientSMTP{
		auth: sasl.NewPlainClient("", email, pwd),
		from: email,
		smtpServer: cfg.Addr,
	}
}

func (cs *ClientSMTP) SendMail(to []string, body []byte) error {
	if err := smtp.SendMail(cs.smtpServer, cs.auth, cs.from, to, bytes.NewBuffer(body)); err != nil {
		return err
	}
	return nil
}