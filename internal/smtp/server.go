package smtp

import (
	"context"
	"io"
	"io/ioutil"
	"log"
	"time"

	"github.com/emersion/go-smtp"
	"gitlab.com/abdr2307/aitu-hack/internal/kafka"
	"gitlab.com/abdr2307/aitu-hack/internal/models"
)

type ServerSMTPConfig struct {
	Addr   string `yaml:"addr"`
	Domain string `yaml:"domain"`
}

func InitServer(conf ServerSMTPConfig, mailRepo *kafka.MailRepository) *smtp.Server {
	be := &Backend{
		mailRepo: mailRepo,
	}

	s := smtp.NewServer(be)

	s.Addr = conf.Addr
	s.Domain = conf.Domain
	s.ReadTimeout = 10 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxMessageBytes = 1024 * 1024
	s.MaxRecipients = 50
	s.AllowInsecureAuth = true

	return s
}

// The Backend implements SMTP server methods.
type Backend struct {
	mailRepo *kafka.MailRepository
}

// Login handles a login command with username and password.
func (bkd *Backend) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	return &Session{
		mailRepo: bkd.mailRepo,
	}, nil
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails
func (bkd *Backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return nil, smtp.ErrAuthRequired
}

// A Session is returned after successful login.
type Session struct {
	from     string
	to       string
	mailRepo *kafka.MailRepository
}

func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	log.Println("Mail from:", from)
	s.from = from
	return nil
}

func (s *Session) Rcpt(to string) error {
	log.Println("Rcpt to:", to)
	s.to = to
	return nil
}

func (s *Session) Data(r io.Reader) error {
	if b, err := ioutil.ReadAll(r); err != nil {
		return err
	} else {
		s.mailRepo.Create(context.TODO(), models.Mail{
			From: s.from,
			To:   s.to,
			Data: string(b),
		})
	}

	return nil
}

func (s *Session) Reset() {
	s.from = ""
	s.to = ""
}

func (s *Session) Logout() error {
	return nil
}
