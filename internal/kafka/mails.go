package kafka

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/abdr2307/aitu-hack/internal/models"
)


type MailRepository struct {
	Sessions map[string][]*models.Socket
	producer *KafkaProducer
}

type event struct {
	Type       string      `json:"type"`
	Created_at time.Time   `json:"created_at"`
	Value      models.Mail `json:"value"`
}

func NewMail(producer *KafkaProducer) *MailRepository {
	return &MailRepository{
		Sessions: make(map[string][]*models.Socket),
		producer: producer,
	}
}

func (mr *MailRepository) Create(ctx context.Context, mail models.Mail) error {
	return mr.publish(ctx, "mail.event.received", mail)
}

func (mr *MailRepository) AddSession(ctx context.Context, username string, sess *models.Socket) {
	mr.Sessions[username] = append(mr.Sessions[username], sess)
	fmt.Println("added")
	for v := range mr.Sessions {
		fmt.Println(v)
	}
}

func (mr *MailRepository) publish(ctx context.Context, msgType string, mail models.Mail) error {
	var b bytes.Buffer

	evt := event{
		Type:  msgType,
		Created_at: time.Now(),
		Value: mail,
	}

	if err := json.NewEncoder(&b).Encode(evt); err != nil {
		return fmt.Errorf("json.Encode: %w", err)
	}

	for v := range mr.Sessions {
		fmt.Println(v)
	}
	for sess := range mr.Sessions[mail.To] {
		fmt.Println(sess)
		mr.Sessions[mail.To][sess].Write(&mail)
	}

	mr.producer.Producer.SetWriteDeadline(time.Now().Add(10 * time.Second))

	if _, err := mr.producer.Producer.Write(b.Bytes()); err != nil {
		return fmt.Errorf("producer.Produce: %w", err)
	}

	return nil
}
