package kafka

import (
	"context"
	"errors"
	"fmt"

	kafka "github.com/segmentio/kafka-go"
)

const (
	producerType = "producer"
	consumerType = "consumer"
)

type KafkaConfig struct {
	Type  string `yaml:"type"`
	Host  string `yaml:"host"`
	Topic string `yaml:"topic"`
}

type KafkaProducer struct {
	Producer *kafka.Conn
	Topic    string
}

var ErrNotEnough = errors.New("Insufficient envs")
var ErrType = errors.New("incorrect type")

func NewKafkaProducer(conf *KafkaConfig) (*KafkaProducer, error) {
	if conf.Type != producerType {
		return nil, ErrType
	}
	host, topic, err := newKafkaConfig(conf)
	if err != nil {
		return nil, fmt.Errorf("newKafkaConfig %w", err)
	}


	client, err := kafka.DialLeader(context.TODO(), "tcp", host, topic, 0)
	if err != nil {
		return nil, fmt.Errorf("kafka.NewProducer %w", err)
	}

	return &KafkaProducer{
		Producer: client,
		Topic:    topic,
	}, nil
}

type KafkaConsumer struct {
	Consumer *kafka.Conn
}

func NewKafkaConsumer(conf *KafkaConfig, groupID string) (*KafkaConsumer, error) {
	if conf.Type != consumerType {
		return nil, ErrType
	}
	host, topic, err := newKafkaConfig(conf)
	if err != nil {
		return nil, fmt.Errorf("newKafkaConfig %w", err)
	}


	client, err := kafka.DialLeader(context.TODO(), "tcp", host, topic, 0)
	if err != nil {
		return nil, fmt.Errorf("kafka.NewConsumer %w", err)
	}

	if _, err := client.Seek(0, kafka.SeekAbsolute); err != nil {
		return nil, fmt.Errorf("client.Subscribe %w", err)
	}

	return &KafkaConsumer{
		Consumer: client,
	}, nil
}


func newKafkaConfig(conf *KafkaConfig) (host, topic string, err error) {
	host, topic = conf.Host, conf.Topic
	if host == "" || topic == "" {
		return "", "", ErrNotEnough
	}

	return host, topic, nil
}
