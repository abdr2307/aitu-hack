package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"gitlab.com/abdr2307/aitu-hack/internal/config"
	"gitlab.com/abdr2307/aitu-hack/internal/kafka"
	"gitlab.com/abdr2307/aitu-hack/internal/models"
	"gitlab.com/abdr2307/aitu-hack/internal/smtp"
)

type MailHandler struct {
	path string
	mr *kafka.MailRepository
}

func NewMailHandler(conf *config.Config, mr *kafka.MailRepository) *MailHandler {
	return &MailHandler{
		path: conf.BasePath,
		mr: mr,
	}
}

func (mh *MailHandler) Path() string {
	return mh.path
}

func (mh *MailHandler) Group(r *gin.RouterGroup) {
	r.POST("/mail", mh.SendMail)
	r.GET("/ws/mail", mh.MailsWS)
}

// SendMail
// @Title        sends mail through api
// @Description  Sends mail with From, To, Subject params
// @Param        mail-info  body  models.SendMailRequest  true  "Mail info"
// @Success      201
// @Failure      422  {object}  models.ErrHandler
// @Failure      400  {object}  models.ErrHandler
// @Failure      500  {object}  models.ErrHandler
// @Router       /mail [post].
func (mh *MailHandler) SendMail(cg *gin.Context) {
	body := cg.Request.Body
	defer body.Close()

	var sendMailRequest models.SendMailRequest

	if err := json.NewDecoder(body).Decode(&sendMailRequest); err != nil {
		log.Err(err)
		cg.JSON(http.StatusUnprocessableEntity, models.ErrHandler{Error: err.Error()})
		return
	}

	client := smtp.NewClientSMTP(sendMailRequest.From, sendMailRequest.Password, &config.Get().SMTPConf)

	if err := client.SendMail([]string{sendMailRequest.To}, []byte(sendMailRequest.Data)); err != nil {
		cg.JSON(http.StatusBadRequest, models.ErrHandler{Error: err.Error()})
		return
	}

	cg.JSON(http.StatusCreated, "")
}

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// MailWS
// @Title        WS for mail receiver
// @Param        email  query  string  true  "Email subscriber"
// @Router       /ws/mail [post].
func (mh *MailHandler) MailsWS(cg *gin.Context) {
	email := cg.Query("email")

	ws, err := upGrader.Upgrade(cg.Writer, cg.Request, nil)
	if err != nil {
		log.Err(err).Msg("MailsWS")
	}
	defer ws.Close()

	sess := models.Socket{
		C: make(chan *models.Mail, 10),
		Done: make(chan struct{}),
	}

	mh.mr.AddSession(cg.Request.Context(), email, &sess)

	log.Info().Msg("ws started with "+ cg.ClientIP())

	for mail := range sess.C {
		log.Info().Msg(mail.To)
		mail.Data = fmt.Sprintf(data, mail.From, mail.To, cg.ClientIP(), mail.Data)
		ws.WriteJSON(mail)
	}
}

var data = `
Date: Tue, 15 Mar 2022 11:28:10 +0600 (ALMT)
From: %s
To: %s
Subject:
Content-Type: text/plain;charset=utf-8

Content-Transfer-Encoding: quoted-printable

Incident Detail
Incident Time: Tue Mar 15 11:28:00 ALMT 2022
Event Severity: 7
Incident Count: 1
Incident Category: Change/UserAccount
Rule
Rule Name: Windows Groups Changed
Remediation:=20
Rule Description:=20
Incident Source
Hostname: ms.STAT.KZ
Source IP:=20
Incident Target
Incident Target: Host Name: ms.HACK.KZ
User: admin_jack
Domain: HACK
Target User Group: Sector14
Target Domain: STAT
Incident Title
Incident Title: Windows group Sector14 modified by admin_jack on ms.HACK.KZ
Organization
Organization Name: Super
Raw Events
Raw: [UserFilter]2022-03-15T05:17:43Z ms.HACK.KZ %s AccelOps-WUA-WinLog-Security [phCustId]=3D"1" [customer]=3D"Super" [monitorStatus]=3D"Success" [Locale]=3D"ru-RU" [MachineGuid]=3D"816ad571-8d03-74f2-a2ca-56431fafc550" [timeZone]=3D"+0600" [eventName]=3D"Security" [eventSource]=3D"Microsoft-Window=s-Security-Auditing" [eventId]=3D"4737" [eventType]=3D"Information" [domain]=3D"" [computer]=3D"ms.HACK.KZ" [user]=3D"" [userSID]=3D"" [userSIDAcctType]=3D"" [eventTime]=3D"Mar 15 2022 05:17:43" [deviceTime]=3D"Mar 15 2022 05:17:43" [msg]=3D"A security-enabled global group was changed." [[Subject]][Security ID]=3D"S-1-5-21-2774759480-1943394314-2232939715-17373" [Account Name]=3D"admin_jack"
Data: %s`