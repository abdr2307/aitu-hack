package handlers

import (
	"gitlab.com/abdr2307/aitu-hack/internal/config"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type SwaggerHandler struct {
	path      string
	FilesPath string
}

func NewSwaggerHandler(conf *config.Config) *SwaggerHandler {
	return &SwaggerHandler{
		path:      "/swagger",
		FilesPath: conf.FilesConf.FilesPath,
	}
}

func (sw *SwaggerHandler) Path() string {
	return sw.path
}

func (sw *SwaggerHandler) Group(rg *gin.RouterGroup) {
	rg.GET("/*any", ginSwagger.WrapHandler(
		swaggerfiles.Handler,
		ginSwagger.URL(sw.FilesPath+"/swagger/swagger.json"),
	))
}
