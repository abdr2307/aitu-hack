package handlers

import (
	"gitlab.com/abdr2307/aitu-hack/internal/config"
	"github.com/gin-gonic/gin"
)

type FSHandler struct {
	filesDir string
	path     string
}

func NewFSHandler(cfg *config.Config) *FSHandler {
	return &FSHandler{
		filesDir: cfg.FilesConf.FilesDir,
		path:     cfg.FilesConf.FilesPath,
	}
}

func (fr *FSHandler) Path() string {
	return fr.path
}

func (fr *FSHandler) Group(r *gin.RouterGroup) {
	fs := gin.Dir(fr.filesDir, true)
	r.StaticFS("/", fs)
}
