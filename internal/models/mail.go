package models

type Mail struct {
	From string `json:"from"`
	To   string `json:"to"`
	Data string `json:"data"`
}

type SendMailRequest struct {
	From     string `json:"from"`
	To       string `json:"to"`
	Password string `json:"password"`
	Subject  string `json:"subject,omitempty"`
	Data     string `json:"data"`
}

type ErrHandler struct {
	Error string `json:"error"`
}

type MailUser struct {
	Email string `json:"email"`
}

type Socket struct {
	closed bool
	C chan *Mail
	Done chan struct{}
}

func (s *Socket) Close() {
	s.Done <- struct{}{}
	s.closed = true
	close(s.C)
}

func (s *Socket) Write(mail *Mail) {
	if s.closed {
		return 
	}
	s.C <- mail
}