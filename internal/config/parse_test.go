package config_test

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/abdr2307/aitu-hack/internal/config"
	"github.com/stretchr/testify/assert"
)

func TestParsedConfig(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	type args struct {
		path string
		c    config.Config
	}
	tests := []struct {
		name    string
		args    args
		before  func()
		assert  func(args) error
		wantErr bool
	}{
		{
			name: "existing file",
			args: args{
				"../../config/config.yml",
				config.Config{},
			},
			wantErr: false,
		},
		{
			name: "not existing file",
			args: args{
				"config/not_existing.yml",
				config.Config{},
			},
			wantErr: true,
		},
		{
			name: "correct env",
			before: func() {
				os.Clearenv()
				os.Setenv("BASE_PATH", "first")
			},
			args: args{
				"../../config/config.yml",
				config.Config{},
			},
			assert: func(a args) error {
				assert.Equal("first", a.c.BasePath, "Should be equal")
				return nil
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.before != nil {
				tt.before()
			}
			if err := config.ParsedConfig(tt.args.path, &tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("ParsedConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Printf("%+v\n", tt.args.c)
			if tt.assert != nil {
				_ = tt.assert(tt.args)
			}
		})
	}
}
