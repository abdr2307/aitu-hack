package config

import (
	"errors"
	"fmt"
	"os"
	"sync"

	"github.com/rs/zerolog/log"
	"gitlab.com/abdr2307/aitu-hack/internal/kafka"
	"gitlab.com/abdr2307/aitu-hack/internal/smtp"
)

type ErrConfig struct {
	err error
}

func (e ErrConfig) Error() string { return fmt.Errorf("config: %w", e.err).Error() }

type Config struct {
	RunMode      string                `yaml:"run_mode" env:"RUN_MODE" env-default:"dev"`
	PostgresConf PostgresConfig        `yaml:"db" env-prefix:"DB_"`
	FilesConf    FilesConfig           `yaml:"files" env-prefix:"FILE_"`
	KafkaConf    kafka.KafkaConfig     `yaml:"kafka"`
	SMTPConf     smtp.ServerSMTPConfig `yaml:"smtp_server"`
	ListenAddr   string                `yaml:"addr" env:"LISTEN_ADDR" env-description:"listen address of the server as an example"` //no-lint:govet
	KeyFile      string                `yaml:"cert_file" env:"KEY_FILE"`
	CertFile     string                `yaml:"key_file" env:"CERT_FILE"`
	BasePath     string                `yaml:"basepath" env:"BASE_PATH" env-description:"base path for the service" env-default:""`
}

type FilesConfig struct {
	FilesDir  string `yaml:"dir" env:"DIR" env-description:"location for static files" env-default:"/files"`
	FilesPath string `yaml:"path" env:"PATH" env-description:"serve path of files" env-default:"/static"`
	FilesSize int    `yaml:"size" env:"SIZE" env-description:"file size that could be uploaded, in MB" env-default:"20"`
}

type PostgresConfig struct {
	Username string `yaml:"user" env:"USER" env-description:""`
	Password string `yaml:"password" env:"PWD" env-description:""`
	Address  string `yaml:"addr" env:"ADDR" env-description:""`
	DB       string `yaml:"dbname" env:"NAME" env-description:""`
}

var conf *Config
var once sync.Once

func New(path string) *Config {
	once.Do(func() {
		var c Config
		if err := ParsedConfig(path, &c); err != nil {
			log.Err(err).Msg("Config.New: ")
			if errors.Is(err, ErrConfig{}) {
				os.Exit(1)
			}
		}
		conf = &c
	})

	return conf
}

func Get() *Config {
	return conf
}
