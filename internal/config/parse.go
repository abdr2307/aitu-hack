package config

import "github.com/ilyakaznacheev/cleanenv"

// ParsedConfig applies config from the file and env to the variable.
func ParsedConfig(path string, c interface{}) error {
	err := cleanenv.ReadConfig(path, c)
	if err != nil {
		return ErrConfig{err}
	}
	return nil
}
