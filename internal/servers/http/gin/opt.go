package gin

import "github.com/gin-gonic/gin"

type Option func(*Server)

func WithGinResources(sg ...SetupGin) Option {
	return func(s *Server) {
		s.setups = append(s.setups, sg...)
	}
}

func WithGinMiddlewares(mds ...gin.HandlerFunc) Option {
	return func(s *Server) {
		s.router.Use(mds...)
	}
}
