package gin

import (
	"gitlab.com/abdr2307/aitu-hack/internal/servers/http"
	"github.com/gin-gonic/gin"
)

type SetupGin interface {
	http.Pather
	// Registers handlers
	Group(*gin.RouterGroup)
}
