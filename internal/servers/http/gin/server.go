package gin

import (
	"context"

	"gitlab.com/abdr2307/aitu-hack/internal/config"
	baseHTTP "gitlab.com/abdr2307/aitu-hack/internal/servers/http"
	"github.com/gin-gonic/gin"
)

type Server struct {
	baseHTTP *baseHTTP.Server
	router   *gin.Engine
	setups   []SetupGin
}

func New(ctx context.Context, conf *config.Config, opts ...Option) *Server {
	baseServer := baseHTTP.New(ctx, conf)

	srv := &Server{
		baseHTTP: baseServer,
		router:   gin.Default(),
	}

	for _, opt := range opts {
		opt(srv)
	}

	srv.router.MaxMultipartMemory = int64(conf.FilesConf.FilesSize) << 20
	srv.router.RedirectTrailingSlash = true
	srv.router.RedirectFixedPath = true

	return srv
}

func (s *Server) Run() error {
	for _, setup := range s.setups {
		group := s.router.Group(setup.Path())
		setup.Group(group)
	}

	s.baseHTTP.SetHandler(s.router)

	if err := s.baseHTTP.Run(); err != nil {
		return err
	}

	return nil
}

func (s *Server) Wait() error {
	return s.baseHTTP.Wait()
}
