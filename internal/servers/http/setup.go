package http

import "net/http"

type Pather interface {
	Path() string
}

type SetupDefault interface {
	Pather
	Handle() http.Handler
}
