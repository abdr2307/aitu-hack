package http

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/abdr2307/aitu-hack/internal/config"
)

type Server struct {
	Address           string
	CertFile, KeyFile string
	RunMode           string

	srv    *http.Server
	setups []SetupDefault

	BaseCtx context.Context
}

func New(ctx context.Context, conf *config.Config) *Server {
	srv := &Server{
		Address:  conf.ListenAddr,
		CertFile: conf.CertFile,
		KeyFile:  conf.KeyFile,
		RunMode:  conf.RunMode,
		BaseCtx:  ctx,
		srv:      &http.Server{},
	}
	baseHandler := http.NewServeMux()

	for _, setup := range srv.setups {
		baseHandler.Handle(setup.Path(), setup.Handle())
	}

	srv.srv.Handler = baseHandler

	return srv
}

func (s *Server) Run() error {
	const (
		readTimeout  = 30 * time.Second
		writeTimeout = 30 * time.Second
	)

	s.srv.ReadTimeout = readTimeout
	s.srv.WriteTimeout = writeTimeout
	s.srv.Addr = s.Address

	if len(s.CertFile) == 0 || len(s.KeyFile) == 0 {
		if err := s.srv.ListenAndServe(); err != nil {
			return err
		}
	} else {
		if err := s.srv.ListenAndServeTLS(s.CertFile, s.KeyFile); err != nil {
			return err
		}
	}

	return nil
}

func (s *Server) Wait() error {
	<-s.BaseCtx.Done()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		return err
	}
	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.srv.Shutdown(ctx)
}

func (s *Server) SetHandler(handler http.Handler) {
	s.srv.Handler = handler
}
