package main

import (
	"bytes"
	"io"
	"log"
	"os"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

// const smtpServer = "smtp.gmail.com:587"
const smtpServer = "localhost:5001"

type ClientSMTP struct {
	auth sasl.Client
	from string
}

func NewClientSMTP(email, pwd string) *ClientSMTP {
	return &ClientSMTP{
		auth: sasl.NewPlainClient("", email, pwd),
		from: email,
	}
}

func (cs *ClientSMTP) SendMail(to []string, body []byte) {
	if err := smtp.SendMail(smtpServer, cs.auth, cs.from, to, bytes.NewBuffer(body)); err != nil {
		log.Println(err)
	}
}

func main() {
	// email := "neversi123123@gmail.com"
	// pwd := "Mcdemon123"
	email := "username"
	to := "neversi123123@gmail.com"
	pwd := "password"

	client := NewClientSMTP(email, pwd)

	file, err := os.Open("example.mail")
	if err != nil {
		log.Fatal(err)
	}

	body, err := io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	client.SendMail([]string{to}, body)
}
