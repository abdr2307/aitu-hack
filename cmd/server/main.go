package main

import (
	"context"
	"flag"

	"github.com/rs/zerolog/log"
	"gitlab.com/abdr2307/aitu-hack/internal/config"
	"gitlab.com/abdr2307/aitu-hack/internal/handlers"
	"gitlab.com/abdr2307/aitu-hack/internal/kafka"
	"gitlab.com/abdr2307/aitu-hack/internal/middlewares"
	ginServer "gitlab.com/abdr2307/aitu-hack/internal/servers/http/gin"
	"gitlab.com/abdr2307/aitu-hack/internal/smtp"
)

const defaultConf = "config/config.yml"
const dev = "dev"

var configFile string

func main() {
	flag.StringVar(&configFile, "c", defaultConf, "conf path")
	flag.Parse()
	conf := config.New(defaultConf)

	producer, err := kafka.NewKafkaProducer(&conf.KafkaConf)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	mr := kafka.NewMail(producer)
	srv := smtp.InitServer(conf.SMTPConf, mr)
	srv.Addr = conf.SMTPConf.Addr
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal().Msg(err.Error())
		}
	}()

	ctx, _ := context.WithCancel(context.Background())

	swaggerHandler := handlers.NewSwaggerHandler(conf)
	fsHandler := handlers.NewFSHandler(conf)

	var ginHandlers []ginServer.SetupGin

	if conf.RunMode == dev {
		ginHandlers = append(ginHandlers,
			swaggerHandler,
			fsHandler,
		)
	}
	serverOpts := ginServer.WithGinResources(
		append(ginHandlers,
			handlers.NewMailHandler(conf, mr),
		)...,
	)

	middlewares := ginServer.WithGinMiddlewares(
		middlewares.CorsMiddleware(),
	)

	server := ginServer.New(ctx, conf, serverOpts, middlewares)

	if err := server.Run(); err != nil {
		log.Err(err).Msg("SERVER ERROR")
	}
}
