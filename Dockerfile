FROM golang:1.17.8-buster as builder
WORKDIR /build

COPY . .
RUN ls
RUN go mod tidy
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -o app ./cmd/server/main.go

# executable
ENTRYPOINT [ "./app" ]
