
kafka:
	docker-compose --build up kafka

main:
	docker-compose -f docker-compose.yml up

thehive:
	docker-compose -f docker-compose-thehive.yml up
	
docs:
	swag init --parseDependency --parseDepth 10 -g cmd/server/doc.go -o static/swagger -ot "json" -d ./ 
